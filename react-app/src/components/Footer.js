export default function Footer(){
    return (
        <div className="mt-4 pt-5 ">
            <h1 className="text-white">Space</h1>
            <h1 className="text-white">Space</h1>
            <h6 className="text-white">Space</h6>
            <h6 className="text-white">Space</h6>
            <p className="text-dark text-center">&copy; 2022 Bits N Bytes / Aljohn Percano</p>
        </div>
    )
}
