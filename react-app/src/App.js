import { useState, useEffect } from 'react';
import { BrowserRouter as Router} from 'react-router-dom';
import { Routes, Route, Navigate } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import AppNavbar from './components/AppNavbar';
import ProductCard from './components/ProductCard';
import Footer from './components/Footer';
import UpdateComponent from './components/UpdateComponent';

import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import Admin from './pages/Admin';
import AddProduct from './pages/AddProduct';
import AllProduct from './pages/AllProduct';
import Update from './pages/Update'

import { UserProvider } from './UserContext';

import './App.css';


function App() {

  const [user, setUser] = useState({
      id: null,
      name: null,
      email: null,
      shopName: null,
      role: null
    });

    const unsetUser = () => {
      localStorage.clear()
    };

    useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {
        if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            name: data.name,
            email: data.email,
            isAdmin: data.isAdmin
          })
        }else{
          setUser({
            id: null,
            name: null,
            email: null,
            isAdmin: null
          })
        }
      })
      .catch(error => console.log(error))
    }, []);




  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/update/:productId" element={<UpdateComponent />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} /> 
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/admin/addProduct" element={<AddProduct/>} />
            <Route path='/admin/allProducts' element={<AllProduct />} />
          </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
