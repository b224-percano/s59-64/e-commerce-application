import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";


export default function Logout() {

	const { unsetUser, setUser } = useContext(UserContext);
	    unsetUser()

	    useEffect(() => {
	        setUser({
	            id: null,
	            name: null,
	            email: null,
	            isAdmin: null
	        })
	        Swal.fire({
	            title: "Logout Successful",
	            icon: "success",
	            text: "You have successfully logout"
	        })
	    }, [setUser])

	return(
		<Navigate to='/' />
	)
}