import { useState, useEffect, useContext } from 'react';
import { Table, Row, Col, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import AllProductsTable from '../components/AllProductsTable';
import UpdateComponent from '../components/UpdateComponent';
import UserContext from '../UserContext';


export default function AllProduct(){

    const { user } = useContext(UserContext);
    const [allProducts, setAllProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setAllProducts(data.map(product => {
                return (
                    <AllProductsTable key={product._id} productProp={product} />
                )
            }))
        })
        .catch(error => console.log(error))
    }, [])

    return (
        (user.isAdmin === false) ?
            <Navigate to='/' />
        :
            <>
                <Row>
                    <Col xs={12}>
                    <h1 id="adminWord" className="text-center pt-3 pb-3">All Products</h1>
                    	<Container>
	                        <Table striped bordered hover variant="dark" className="allProductTable text-center">
	                            <thead>
	                                <tr>
	                                    <th>NAME</th>
	                                    <th>PRICE</th>
	                                    <th>STOCK</th>
	                                    <th>SOLD</th>
	                                    <th>AVAILABILITY</th>
	                                    <th>ACTIONS</th>
	                                </tr>
	                            </thead>
	                            {allProducts}
	                        </Table>
	                    </Container>
                    </Col>
                </Row>
            </>
    )
};
